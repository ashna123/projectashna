from Product.models import Category, Products
from django.contrib import admin

admin.site.register(Category)
admin.site.register(Products)
