from django.db import models
from django.db.models.deletion import CASCADE
from vendor.models import Vendor

class Category(models.Model):
    category_name=models.CharField(max_length=40,null=True)
    category_desc=models.CharField(max_length=100,null=True)

    def __str__(self):
        return self.category_name

class Products(models.Model):
    product_name=models.CharField(max_length=40,null=True)
    product_image=models.ImageField(upload_to='Pictures/',null=True,blank=True)
    product_price=models.IntegerField()
    product_stock=models.IntegerField()
    product_stock_status=models.BooleanField(default=False,null=True)
    product_desc=models.CharField(max_length=100,null=True)
    category=models.ForeignKey(Category,on_delete=models.CASCADE,null=True)
    vendor=models.ForeignKey(Vendor,on_delete=CASCADE,null=True)

    def __str__(self):
        return self.product_name