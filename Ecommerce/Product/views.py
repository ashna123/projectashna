from pathlib import Path
from django.shortcuts import render, redirect
from .models import Category, Products
from django.http.response import HttpResponse
from django.core.files.storage import FileSystemStorage
from ekart.models import User,order
from vendor.models import Vendor
from django.contrib.auth import logout, authenticate, login
import pandas as pd
import os
BASE_DIR = Path(__file__).resolve().parent.parent
from django.core.files import File
# DashBoard

def dashboard(request):
        return render(request,"dashboard.html")


# DashBoard Front

def dashboardfront(request):
    pc=Products.objects.all().count()
    uc=User.objects.all().count()
    oc=order.objects.all().count()
    vc=Vendor.objects.all().count()
    return render(request,"dashboardfront.html",{'productcount':pc,'usercount':uc,'ordercount':oc,'vendorcount':vc})

# Add Category

def addcategory(request):
    
    if request.method=="POST":
        cna=request.POST.get("category_name")
        cde=request.POST.get("category_desc")
        if Category.objects.filter(category_name=cna).exists():
            return HttpResponse("Category already Exists")
        else:
            Category.objects.create(category_name=cna,category_desc=cde)
            return redirect('addproducts')
    return render(request,"category.html")

# List Category

def listcategory(request):
    c=Category.objects.all()
    return render(request,'listcategory.html',{'cat':c})

# Add Product

def addproducts(request):
    c=Category.objects.all()
    v=Vendor.objects.all()
    if request.method=="POST":
        na=request.POST.get("product_name")
        cl=request.POST.get("product_price")
        m1=request.POST.get("product_stock")
        m2=request.POST.get("product_stock_status")
        m3=request.POST.get("product_desc")
        im=request.FILES['product_image']
        ca=request.POST.get("category")
        ve=request.POST.get("vendor")
        f=FileSystemStorage()
        fp=f.save(im.name,im)
        b=Category.objects.get(category_name=ca)
        vb=Vendor.objects.get(username=ve)
        Products.objects.create(product_name=na,product_price=cl,product_stock=m1,product_stock_status=m2,product_desc=m3,product_image=fp,category=b,vendor=vb)
    return render(request,"product.html",{'cat':c,'ven':v})

# List Products

def listproduct(request):
    k=Products.objects.all()
    df=pd.DataFrame(Products.objects.all().values())
    path=os.path.join(BASE_DIR,'media/csvfile/products.csv')
    df.to_csv(path)
    return render(request,'listproduct.html',{'pro':k})

# Filter Products by Category

def filterCat(request):
        if request.method=="POST":
            na=request.POST.get("searchcat")
            sa=Products.objects.filter(category__category_name=na)
            df=pd.DataFrame(sa.values())
            path=os.path.join(BASE_DIR,'media/csvfile/products.csv')
        df.to_csv(path)
        return render(request,"listproduct.html",{'pro':sa})

# Filter Products By Name

def filterName(request):
    if request.method=="POST":
        na=request.POST.get("searchname")
        pa=Products.objects.filter(product_name=na)
        df=pd.DataFrame(pa.values())
        path=os.path.join(BASE_DIR,'media/csvfile/products.csv')
    df.to_csv(path)
    return render(request,"listproduct.html",{'pro':pa})

# Update Products

def updateproduct(request,userid):
        c=Category.objects.all()
        k=Products.objects.filter(id=userid).values()

    
        if request.method=="POST":
            na=request.POST.get("product_name")
            cl=request.POST.get("product_price")
            m1=request.POST.get("product_stock")
            m2=request.POST.get("product_stock_status")
            m3=request.POST.get("product_desc")
            im=request.FILES['product_image']
            ca=request.POST.get("category")
            f=FileSystemStorage()
            fp=f.save(im.name,im)
            b=Category.objects.get(category_name=ca)
            k.update(product_name=na,product_price=cl,product_stock=m1,product_stock_status=m2,product_desc=m3,product_image=fp,category=b)
            return redirect("addproducts")
        return render(request,"updateproduct.html",{"pro":k[0],"id":userid,"cat":c})

# Delete Products

def delproduct(request,userid):
    k=Products.objects.filter(id=userid)
    k.delete()
    return redirect("listproduct")

# Add User

def registerad(request):
    u=User.objects.all()
    if request.method=="POST":
        una=request.POST.get("username")
        fna=request.POST.get("first_name")
        lna=request.POST.get("last_name")
        em=request.POST.get("email")
        mo=request.POST.get("phone")
        add=request.POST.get("address")
        ro=request.POST.get("role")
        ci=request.POST.get("city")
        pc=request.POST.get("postcode")
        p=request.POST.get("password")
        im=request.FILES['profilepic']
        f=FileSystemStorage()
        fp=f.save(im.name,im)
        User.objects.create_user(username=una, first_name=fna, last_name=lna, email=em, address=add, role=ro, city=ci, postcode=pc, phone=mo, password=p, profilepic=fp)
    return render(request,"adduser.html",{'user':u})

# List Users

def listusers(request):
    u=User.objects.all()
    return render(request,"listuser.html",{'user':u})

# filter User By Roles

def filterUserRoles(request):
    r=request.POST.get("searchroles")
    k=User.objects.filter(role=r)
    return render(request,"listuser.html",{'user':k})

# Login

def loginadmin(request):
    if request.method=="POST":
        em=request.POST.get("useremail")
        pw=request.POST.get("userpassword")
        ppp=User.objects.get(email=em)
        user=authenticate(request,email=em, password=pw)
        if user:
            login(request,user)
            print(request.user)
            if ppp.role=='admin':
                return redirect('dashboardfront')
            else:
                return redirect('home')
        else:
            return render(request,"adminlogin.html")
    return render(request,'adminlogin.html')

# Logout

def signoutadmin(request):
    logout(request)
    return redirect('loginadmin')

# Product Report

def productreport(request):
    k=Products.objects.all()
    v=Vendor.objects.all()
    df=pd.DataFrame(Products.objects.all().values('product_name','category__category_name','vendor__username','product_stock'))
    path=os.path.join(BASE_DIR,'media/csvfile/products.pdf')
    df.to_csv(path)
    if request.method=='POST':
        sc=request.POST.get("searchcategory")
        sv=request.POST.get("searchvendor")
        sn=request.POST.get("searchname")
        if(sc !=""):
            fil=Products.objects.filter(category__category_name=sc)
        
        if(sv !=""):
            fil=Products.objects.filter(vendor__username=sv)
        
        if(sn !=""):
            fil=Products.objects.filter(product_name=sn)

        if(sc !="" and sv !=""):
            fil=Products.objects.filter(category__category_name=sc,vendor__username=sv)

        if(sc !="" and sn !=""):
            fil=Products.objects.filter(category__category_name=sc,product_name=sn)

        if(sv !="" and sn !=""):
            fil=Products.objects.filter(vendor__username=sv,product_name=sn)

        if(sc !="" and sv !="" and sn !=""):
            fil=Products.objects.filter(category__category_name=sc,vendor__username=sv,product_name=sn)

        df=pd.DataFrame(fil.values('product_name','category__category_name','vendor__username','product_stock'))
        path=os.path.join(BASE_DIR,'media/csvfile/products.csv')
        df.to_csv(path)
        return render(request,"productreport.html",{'pro':fil,'ven':v})
    return render(request,"productreport.html",{'pro':k,'ven':v})