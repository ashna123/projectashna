from django.urls import path
from Product import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
   path('dashboard',views.dashboard,name='dashboard'),
   path('dashboardfront',views.dashboardfront,name='dashboardfront'),
   path('',views.loginadmin,name='loginadmin'),
   path('signoutadmin',views.signoutadmin,name="signoutadmin"),
   path('addproducts',views.addproducts,name='addproducts'),
   path('listproduct',views.listproduct,name='listproduct'),
   path('addcategory',views.addcategory,name='addcategory'),
   path('listcategory',views.listcategory,name='listcategory'),
   path('filterCat',views.filterCat,name='filterCat'),
   path('delproduct<int:userid>',views.delproduct,name="del"),
   path('filterName',views.filterName,name='filterName'),
   path('updateproduct<int:userid>',views.updateproduct,name="edit"),
   path('registerad',views.registerad,name='registerad'),
   path('listusers',views.listusers,name='listusers'),
   path('filterUserRoles',views.filterUserRoles,name='filterUserRoles'),
   path('productreport',views.productreport,name='productreport'),
  
] +static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,document_root = settings.STATIC_URL)
    urlpatterns += static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)