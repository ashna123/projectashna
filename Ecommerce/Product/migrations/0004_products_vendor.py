# Generated by Django 3.2.6 on 2021-09-29 09:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('vendor', '0001_initial'),
        ('Product', '0003_alter_products_product_stock_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='products',
            name='vendor',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='vendor.vendor'),
        ),
    ]
