from django.shortcuts import render, redirect
from .models import Vendor

# Add and List Vendors

def addvendors(request):
    if request.method=="POST":
        br=request.POST.get("branch")
        vn=request.POST.get("vendor_name")
        pn=request.POST.get("phone_number")
        em=request.POST.get("email")
        un=request.POST.get("username")
        ps=request.POST.get("password")
        ad=request.POST.get("address")
        Vendor.objects.create(branch=br, vendor_name=vn, phone_number=pn, email=em, username=un, password=ps, address=ad)
    return render(request,"addvendor.html")

# List Vendors

def listvendor(request):
    cc=Vendor.objects.all()
    return render(request,'listvendor.html',{'vendor':cc})

# Delete Vendors

def delvendor(request,userid):
    k=Vendor.objects.filter(id=userid)
    k.delete()
    return redirect("addvendors")