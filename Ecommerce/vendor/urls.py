from django.urls import path
from vendor import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('addvendors',views.addvendors,name='addvendors'),
    path('delvendor<int:userid>',views.delvendor,name='delvendor'),
    path('listvendor',views.listvendor,name='listvendor')
] +static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,document_root = settings.STATIC_URL)
    urlpatterns += static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)