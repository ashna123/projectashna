# Generated by Django 3.2.6 on 2021-10-07 09:20

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Product', '0004_products_vendor'),
        ('ekart', '0010_rename_customer_order_customer_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='addtocart',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField(default=1, null=True)),
                ('cart_product', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='Product.products')),
                ('cart_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
