# Generated by Django 3.2.6 on 2021-09-12 07:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ekart', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Reguser',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=40, null=True)),
                ('username', models.CharField(max_length=40, null=True)),
                ('email', models.EmailField(max_length=50, null=True)),
                ('mobile', models.IntegerField()),
                ('password', models.CharField(max_length=30, null=True)),
                ('profilepic', models.ImageField(blank=True, null=True, upload_to='Images/')),
            ],
        ),
    ]
