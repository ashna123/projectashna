from django.urls import path
from ekart import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('temp',views.temp,name='home'),
    path('register',views.register,name='register'),
    path('signout',views.signout,name="signout"),
    path('login',views.loginuser,name='login'),
    path('updatepass',views.updatepass,name='updatepass'),
    path('myaccount',views.myaccount,name='myaccount'),
    path('addorders',views.addorders,name='addorders'),
    path('listorder',views.listorder,name='listorder'),
    path('delorders<int:userid>',views.delorders,name='delorders'),
    path('filterOrderStatus',views.filterOrderStatus,name='filterOrderStatus'),
    path('filterOrderDate',views.filterOrderDate,name='filterOrderDate'),
    path('filterOrderCid',views.filterOrderCid,name="filterOrderCid"),
    path('listorderdetails',views.listorderdetails,name='listorderdetails'),
    path('productindex',views.productindex,name='productindex'),
    path('filterNameIndex',views.filterNameIndex,name='filterNameIndex'),
    path('filterCatIndex',views.filterCatIndex,name='filterCatIndex'),
    path('addCart<int:userid>',views.addCart,name='addCart'),
    path('showcart',views.showcart,name='showcart'),
    path('detailsProduct<int:pid>',views.detailsProduct,name='detailsProduct'),
    path('delCart<int:pid>',views.delCart,name='delCart'),
    path('checkout',views.checkout,name='checkout'),
    path('addwishlist<int:userid>',views.addwishlist,name='addwishlist'),
    path('showwishlist',views.showwishlist,name='showwishlist'),
    path('delwishlist<int:pid>',views.delwishlist,name='delwishlist'),
    path('placeorder',views.placeorder,name='placeorder'),
] +static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,document_root = settings.STATIC_URL)
    urlpatterns += static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)