from ekart.models import *
from Product.models import Category, Products
from vendor.models import Vendor
from django.shortcuts import render, redirect
from django.http.response import HttpResponse
from django.core.files.storage import FileSystemStorage
from django.contrib.auth import logout, authenticate,login
import datetime
from django.db.models import Sum

# DashBoard
def temp(request):
    uid=request.user.id
    u=User.objects.get(id=uid)
    p=Products.objects.all()
    ct=Category.objects.all()
    atc=addtocart.objects.filter(cart_user=u).count()
    atw=addtowishlist.objects.filter(wishlist_user=u).count()
    return render(request,"index.html",{'cartcount':atc, 'product':p,'wishlistcount':atw,'category':ct})

# My Account Page

def myaccount(request):
    uid=request.user.id
    u=User.objects.get(id=uid)
    atc=addtocart.objects.filter(cart_user=u).count()
    atw=addtowishlist.objects.filter(wishlist_user=u).count()
    return render(request,"my-account.html",{'cartcount':atc,'wishlistcount':atw})

# Product Page

def productindex(request):
    p=Products.objects.all()
    uid=request.user.id
    u=User.objects.get(id=uid)
    atc=addtocart.objects.filter(cart_user=u).count()
    atw=addtowishlist.objects.filter(wishlist_user=u).count()
    return render(request,"product-index.html",{'product':p,'cartcount':atc,'wishlistcount':atw})

# Filter Products By Name in Index

def filterNameIndex(request):
    if request.method=="POST":
        na=request.POST.get("searchname")
        pa=Products.objects.filter(product_name=na)
    return render(request,"product-index.html",{'product':pa})

# Filter Products by Category in Index

def filterCatIndex(request):
    if request.method=="POST":
        na=request.POST.get("searchcat")
        sa=Products.objects.filter(category__category_name=na)
    return render(request,"product-index.html",{'product':sa})

# Register User

def register(request):
    if request.method=="POST":
        una=request.POST.get("username")
        fna=request.POST.get("first_name")
        lna=request.POST.get("last_name")
        em=request.POST.get("email")
        mo=request.POST.get("phone")
        add=request.POST.get("address")
        ro=request.POST.get("role")
        ci=request.POST.get("city")
        pc=request.POST.get("postcode")
        p=request.POST.get("password")
        im=request.FILES['profilepic']
        f=FileSystemStorage()
        fp=f.save(im.name,im)
        User.objects.create_user(username=una, first_name=fna, last_name=lna, email=em, address=add, role=ro, city=ci, postcode=pc, phone=mo, password=p, profilepic=fp)
    return render(request,"login.html")

# User Login
def loginuser(request):
    if request.method=="POST":
        em=request.POST.get("email")
        pw=request.POST.get("password")
        user=authenticate(request,email=em, password=pw)
        if user:
            login(request,user) 
            return render(request,"index.html")
        else:
            return render(request,"login.html")

    return render(request,"login.html")
        
# User Logout

def signout(request):
    logout(request)
    return redirect('login')

# Update Password

def updatepass(request):
    if request.method=="POST":
        npw=request.POST.get("newpassword")
        u=User.objects.get(email=request.user.email)
        u.set_password(npw)
        u.save()
        return redirect('home')
    return render(request,"my-account.html")


# Add and List Orders
def addorders(request):
    c=User.objects.all()
    cc=Vendor.objects.all()
    pp=Products.objects.all()
    if request.method=="POST":
        oda=request.POST.get("order_date")
        cid=request.POST.get("customer_id")
        vid=request.POST.get("vendor_id")
        ot=request.POST.get("order_total")
        pm=request.POST.get("payment_method")
        pi=request.POST.get("product_id")
        pq=request.POST.get("product_quantity")
        ps=request.POST.get("payment_status")
        dd=request.POST.get("delivery_date")
        s=request.POST.get("status")
        idd=request.POST.get("Is_delivered")
        ppp=Products.objects.get(product_name=pi)
        price=ppp.product_price
        st=int(price)*int(pq)
        b=User.objects.get(email=cid)
        d=Vendor.objects.get(username=vid)
        if int(ppp.product_stock) < int(pq):
            return HttpResponse("Out of Stock")
        else:
            ppp.product_stock -= int(pq)
            ppp.save()
            xx=order.objects.create(order_date=oda,order_total=ot,payment_method=pm,payment_status=ps,delivery_date=dd,status=s,Is_delivered=idd, customer_id=b, vendor_id=d )
            orderdetails.objects.create(product_id=ppp, product_qty=pq, product_price=price,subtotal=st, order=xx)
    return render(request,"order.html",{'cid':c,'vid':cc,'product':pp})

# List Orders

def listorder(request):
    c=User.objects.all()
    o=order.objects.all()
    return render(request,"listorder.html",{'pro':o,'cid':c})

# Delete Orders

def delorders(request,userid):
    k=order.objects.filter(id=userid)
    k.delete()
    return redirect("listorder")

# Filter Orders by Customer ID

def filterOrderCid(request):
    c=User.objects.all()
    if request.method=="POST":
        ss=request.POST.get("searchcid")
        k=order.objects.filter(customer_id__email=ss)
        return render(request,"listorder.html",{'pro':k,'cid':c})

# Filter Orders by Status

def filterOrderStatus(request):
    c=User.objects.all()
    if request.method=="POST":
        ss=request.POST.get("searchstatus")
        k=order.objects.filter(status=ss)
        return render(request,"listorder.html",{'pro':k,'cid':c})

# Filter Orders by Delivery Date

def filterOrderDate(request):
    c=User.objects.all()
    if request.method=="POST":
        datefrom=request.POST.get("dfrom")
        dateto=request.POST.get("dto")
        k=order.objects.filter(delivery_date__range=(datefrom,dateto))
        return render(request,"listorder.html",{'pro':k,'cid':c})

# List Order Details

def listorderdetails(request):
    od=orderdetails.objects.all()
    return render(request,'listorderdetails.html',{'orderdetails':od})

# Add Cart

def addCart(request,userid):
    uid=request.user.id
    p=Products.objects.get(id=userid)
    u=User.objects.get(id=uid)
    if request.method=="POST":
        qu=request.POST.get("quantity")
        if int(p.product_stock) < int(qu):
            return HttpResponse("Out of Stock")
        else:
            addtocart.objects.create(cart_user=u, cart_product=p, quantity=qu)
            return redirect("showcart")
    else:
        if int(p.product_stock) < 1:
            return HttpResponse("Out of Stock")
        else:
            addtocart.objects.create(cart_user=u, cart_product=p)
            return redirect("productindex")

# Delete Cart Items

def delCart(request,pid):
    k=addtocart.objects.filter(cart_product_id=pid)
    k.delete()
    return redirect("showcart")

# Product Details Page

def detailsProduct(request,pid):
    uid=request.user.id
    u=User.objects.get(id=uid)
    atc=addtocart.objects.filter(cart_user=u).count()
    atw=addtowishlist.objects.filter(wishlist_user=u).count()
    p=Products.objects.get(id=pid)
    pa=Products.objects.all()
    return render(request,"product-detail.html",{'productdetails':p,'products':pa,'cartcount':atc,'wishlistcount':atw})

# Show Cart

def showcart(request):
    sum=0
    uid=request.user.id
    u=User.objects.get(id=uid)
    ca=addtocart.objects.filter(cart_user__id=uid)
    atc=addtocart.objects.filter(cart_user=u).count()
    atw=addtowishlist.objects.filter(wishlist_user=u).count()
    # sub=addtocart.objects.filter(cart_user__id=uid).aggregate(sum('cart_product__product_price'))['cart_product__product_price__sum']
    for i in ca:
        pn=i.cart_product_id
        q=i.quantity
        pa=Products.objects.get(id=pn)
        price=pa.product_price
        sum=sum+price*q
    return render(request,"cart.html",{'product':ca,'total':sum,'cartcount':atc,'wishlistcount':atw})

# Checkout

def checkout(request):
    cc=Vendor.objects.all()
    sum=0
    uid=request.user.id
    u=User.objects.get(id=uid)
    ca=addtocart.objects.filter(cart_user__id=uid)
    atc=addtocart.objects.filter(cart_user=u).count()
    atw=addtowishlist.objects.filter(wishlist_user=u).count()
    for i in ca:
        pn=i.cart_product_id
        q=i.quantity
        pa=Products.objects.get(id=pn)
        price=pa.product_price
        sum=sum+price*q
    return render(request,"checkout.html",{'product':ca,'vid':cc, 'total':sum,'cartcount':atc,'wishlistcount':atw})

# Add to Wishlist

def addwishlist(request,userid):
    uid=request.user.id
    p=Products.objects.get(id=userid)
    u=User.objects.get(id=uid)
    addtowishlist.objects.create(wishlist_user=u, wishlist_product=p)
    return redirect("productindex")

# Show WishList

def showwishlist(request):
    uid=request.user.id
    u=User.objects.get(id=uid)
    ca=addtowishlist.objects.filter(wishlist_user=u)
    atw=addtowishlist.objects.filter(wishlist_user=u).count()
    atc=addtocart.objects.filter(cart_user=u).count()
    ProductArray=[]
    for i in ca:
        pn=i.wishlist_product_id
        pa=Products.objects.get(id=pn)
        ProductArray.append(pa)
    return render(request,"wishlist.html",{'product':ProductArray,'cartcount':atc,'wishlistcount':atw})

# Delete Wishlist Items

def delwishlist(request,pid):
    k=addtowishlist.objects.filter(wishlist_product_id=pid)
    k.delete()
    return redirect("showwishlist")

# Place Order

def placeorder(request):
    uid=request.user.id
    u=User.objects.get(id=uid)
    ca=addtocart.objects.filter(cart_user=u)
    if request.method=="POST":
        fna=request.POST.get("first_name")
        lna=request.POST.get("last_name")
        em=request.POST.get("email")
        mo=request.POST.get("phone")
        add=request.POST.get("address")
        ci=request.POST.get("city")
        pc=request.POST.get("postcode")
        pm=request.POST.get("payment_method")
        st=request.POST.get("state")
        vname=request.POST.get("vendor_name")
        dd=datetime.date.today()
        ddd=dd+datetime.timedelta(days=4)
        b=User.objects.get(email=u.email)
        d=Vendor.objects.get(vendor_name=vname)
        xx=order.objects.create(order_date=dd,order_total=1,payment_method=pm,payment_status=True,delivery_date=ddd,status="Ordered",Is_delivered=False, customer_id=b, vendor_id=d )
        shipment.objects.create(first_name=fna, last_name=lna, email=em, phone=mo, address=add, city=ci, postcode=pc,state=st, order=xx )
        for i in ca:
            sum=0
            pn=i.cart_product_id
            pa=Products.objects.get(id=pn)
            q=i.quantity
            price=pa.product_price
            sum=int(price)*int(q)
            pa.product_stock -= int(q)
            pa.save()
            orderdetails.objects.create(product_qty=q, product_price=price,subtotal=sum, order=xx)
    return render(request,"checkout.html")